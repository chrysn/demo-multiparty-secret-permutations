Shuffling demo
==============

This is an experimental implementation of
[an algorithm introduced by Studholme and Blake ("Multiparty Computation to Generate Secret Permutations")](https://eprint.iacr.org/2007/353.pdf)
that allows multiple participants to agree on a sequence of the participants,
where each player only knows their own position.

One application of this algorithm is dealing cards in a card game
without a shuffling authority.

This demo does not implement any external communication,
but just runs the algorithm by multiple parties internally
and compares results.
The group over which the ElGamal encryption is run is the [Ristretto](https://ristretto.group/) prime-order group (based on Curve25519).
It should be straightforward to port to other groups;
that one was picked because it has a [well accessible Rust implementation](https://crates.io/crates/curve25519-dalek),
and is a prime group (which the paper does not require, but many other ElGamal applications do, so … unsure, it doesn't hurt, I guess?).

It serves as a way for the author to familiarize himzelf with the topic,
and as an exercise in transferring cryptographic algorithms from paper to code.

It also serves as a playground to explore extensions of the algorithm,
especially

* encrypting some cards with the product of all parties' public keys,
  as to make them unusable
  (think of dealing some cards to the players but keeping the rest in a face-down unused stack),
  implemented in the `cards_to_spare` example,

* encrypting some cards with extra keys (one per party and card),
  and revealing those extra keys to a single party when agreed on
  (think of agreeing in the course of the game on who gets to draw the next card),
  which is not implemented yet.

Usage
-----

To support different demos,
this crate is built as a library.

Examples can be run as `cargo run --example original`
or `cargo run --example cards_to_spare`.
