use demo_multiparty_secret_permutations::*;

fn main() {
    let mut rng = rand_core::OsRng;

    let players = [
        Player::new(&mut rng),
        Player::new(&mut rng),
        Player::new(&mut rng),
        Player::new(&mut rng)
    ];

    run_game_with_cards_to_spare(&players, &mut rng);
}
