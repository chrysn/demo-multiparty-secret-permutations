use curve25519_dalek::{scalar::Scalar, constants::RISTRETTO_BASEPOINT_TABLE, ristretto::{RistrettoPoint, RistrettoBasepointTable}};
use rand_core::{RngCore, CryptoRng};

#[allow(non_upper_case_globals)] // to keep the convention of the paper
static g: RistrettoBasepointTable = RISTRETTO_BASEPOINT_TABLE;

pub struct Player {
    /// Private key revealed after the shuffling phase
    x: Scalar,
    /// Private key retained throughtout the complete process
    u: Scalar,
}

impl Player {
    pub fn new<R: RngCore + CryptoRng>(rng: &mut R) -> Self {
        Player {
            x: Scalar::random(rng),
            u: Scalar::random(rng),
        }
    }

    fn get_public_u(&self) -> RistrettoPoint {
        &g * &self.u
    }

    /// Only call this after the final result has been published
    fn get_private_x(&self) -> Scalar {
        self.x
    }

    fn run_on(&self, ciphertexts: &mut [(RistrettoPoint, RistrettoPoint)], rng: &mut (impl RngCore + CryptoRng))
    {
        for (alpha, beta) in ciphertexts.iter_mut() {
            let r = Scalar::random(rng);
            *alpha = *alpha * r + *beta * self.x * r;
            *beta = *beta * r;
        }

        use rand::seq::SliceRandom;
        ciphertexts.shuffle(rng);
    }

    fn show_result(&self, ciphertexts: &[(RistrettoPoint, RistrettoPoint)], sum_x: Scalar) {
        let w = self.u + sum_x;
        let null = &g * &Scalar::zero();

        for (index, (alpha, beta)) in ciphertexts.iter().enumerate() {
            // FIXME: in the algorithm, there's an assertion on beta != 1; we're in additive
            // notation, does this matter?

            if alpha - beta * w == null {
                println!("Found myself in position {}", index);
            }
        }
    }
}

/// Run the complete algorithm.
///
/// This both runs the steps described for Player 1 and calling the players to run the per-player
/// steps.
///
/// It generally only exposes what it needs via its functions, but does not enforce them being
/// called in the right sequence.
pub fn run_game(players: &[Player], rng: &mut (impl RngCore + CryptoRng)) {
    let mut ciphertexts: Vec<_> = players.iter().map(|p| (
            p.get_public_u(),
            &g * &Scalar::one()
        )).collect();

    // FIXME: missing commitment on x_i

    for p in players {
        p.run_on(&mut ciphertexts, rng);
    }

    let sum_x = players.iter().map(|p| p.get_private_x()).sum();

    for (i, p) in players.iter().enumerate() {
        println!("For player {}:", i);
        p.show_result(&ciphertexts, sum_x);
    }
}

/// Like run_game, but use a combined public key of all players to get some positions that no
/// player can occupy (but won't see they're unoccupied either)
pub fn run_game_with_cards_to_spare(players: &[Player], rng: &mut (impl RngCore + CryptoRng)) {
    let mut ciphertexts: Vec<_> = players.iter().map(|p| (
            p.get_public_u(),
            &g * &Scalar::one()
        )).collect();

    let general_public = players.iter().map(|p| p.get_public_u()).sum();

    ciphertexts.push((general_public, &g * &Scalar::one()));
    ciphertexts.push((general_public, &g * &Scalar::one()));
    ciphertexts.push((general_public, &g * &Scalar::one()));
    ciphertexts.push((general_public, &g * &Scalar::one()));

    // FIXME: missing commitment on x_i

    for p in players {
        p.run_on(&mut ciphertexts, rng);
    }

    let sum_x = players.iter().map(|p| p.get_private_x()).sum();

    for (i, p) in players.iter().enumerate() {
        println!("For player {}:", i);
        p.show_result(&ciphertexts, sum_x);
    }
}
